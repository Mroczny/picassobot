import {
    On,
    ArgsOf,
  } from "@typeit/discord";

  export abstract class Events {
  
    @On("ready")
    initialize(): void {
      console.log("Logged and ready to paint!");
    }
  
    @On("message")
    recievedMessage([message]: ArgsOf<"message">): void {
      console.log("Got message", message.content);
    }
  
    @On("guildCreate")
    guildJoin([guild]: ArgsOf<"guildCreate">): void {
      console.log(`Bot added to the Discord Server : ${guild.name}`);
    }
  
  }

   